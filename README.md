## API endpoints

| APIs                 | VERB | Parameters | Query Params URL | Description        |
|----------------------| --- | --- |------------------|--------------------|
| /api/medias          | GET | (none) | (page, size)     | Get list of media |
| /api/media/:media_id | GET | (media_id) | (none)           | Get media info     |

### Installation
After cloning the repo follow the steps below:

Copy env config `cp .env-example .env`

run `npm install`
# Migrations

### Installing the CLI
To install the Sequelize CLI:
`npm install -g sequelize-cli`

### Configuration
Before continuing further we will need to tell the CLI how to connect to the database. To do that let's open default config or create file config/database.js. It looks something like this:

```
development: {
    "username": "root",
    "password": "AD2Ym9ToH83IXqWiYBHS",
    "database": "railway",
    "port": "6145",
    "host": "containers-us-west-137.railway.app",
    "dialect": "mysql"
  },
  localhost: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    forceSchema: true,
    allowDestructiveSync: true
  },
  test: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
  },
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
  }
```

### Running Migrations

Until this step, we haven't inserted anything into the database. We have just created required model and migration files for our first model User. Now to actually create that table in database you need to run ```npm run db:migrate``` command.
*Will ensure a table called SequelizeMeta in database. This table is used to record which migrations have run on the current database*

### Running Seeds
To do that we need to run a simple command. ```npm run db:seed```


run ```npm start```
