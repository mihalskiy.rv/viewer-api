const { Media } = require('../../../models');
const { getPagination, getPagingData } = require('../../utils/helpers');

exports.getSingleMedia = (req, res) => {
  if (!req.params.id) {
    return res.status(404).send({ error: 'wrong params' });
  }
  Media.findByPk(req.params.id)
    .then((media) => {
      if (!media) {
        return res.status(404).send({ error: 'media not found' });
      }

      return res.status(200).send(media);
    })
    .catch((err) => res.status(400).send({ error: err.message }));
};

exports.getAllMedia = (req, res) => {
  const { page, size } = req.query;
  const { limit, offset } = getPagination(page, size);

  Media.findAndCountAll({ limit, offset, raw: true })
    .then((data) => {
      const response = getPagingData(data, page, limit);

      return res.status(200).send(response);
    })
    .catch((err) => res.status(500).send({ error: err.message || 'Some error occurred while retrieving Media.' }));
};
