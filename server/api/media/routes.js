const router = require('express').Router();
const controller = require('./controller');

router.route('/:id').get(controller.getSingleMedia);
router.route('/').get(controller.getAllMedia);

module.exports = router;
