const router = require('express').Router();

// api router will mount other routers for all our resources
router.use('/medias', require('./media/routes'));

module.exports = router;
