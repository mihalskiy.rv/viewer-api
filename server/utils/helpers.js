exports.getPagination = (page, size) => {
  const limit = size ? +size : 4;
  const offset = page ? page * limit : 0;

  return { limit, offset };
};

exports.getPagingData = (data, page, limit) => {
  const { count: totalItems, rows: medias } = data;
  const currentPage = page ? +page : 0;
  const pages = Math.ceil(totalItems / limit);

  return {
    totalItems, medias, pages, currentPage,
  };
};
