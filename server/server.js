const express = require('express');

const app = express();
const api = require('./api/api');

// Middlewares setup
require('./middlewares')(app);

// Routes
app.use('/api', api);
app.use('/', (req, res) => {
  res.status(200).send('ok');
});
module.exports = app;
