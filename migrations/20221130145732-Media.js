module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.createTable('Media', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      title: {
        type: Sequelize.STRING,
      },
      cost: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.TEXT,
      },
      thumbnail: {
        type: Sequelize.TEXT,
      },
      image: {
        type: Sequelize.TEXT,
      },
    });
  },

  async down(migration, Sequelize, done) {
    migration.dropTable('Media').done(done);
  },
};
